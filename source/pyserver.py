import socket, time

HOST = '127.0.0.1'                 # Symbolic name meaning all available interfaces
PORT = 50007              # Arbitrary non-privileged port
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    connect = False

    s.bind((HOST, PORT))
    s.listen(1)
    print('Server started, waiting for client...')
    while not connect:
        conn, addr = s.accept()
        connect = True
        with conn:
            print('Connected by', addr)
            while True:

                try:
                    data = conn.recv(1024)
                    countermessage = (data.decode() + ' OK')
                    print(1000*time.time())
                    conn.sendall(countermessage.encode() )
                    print ('received: ', data.decode(), '. Sending: ', countermessage)
                except:
                    break
                #if not data: break


            connect = False
            print ('no data received')



